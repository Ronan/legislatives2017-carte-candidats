package fr.partipirate.legislatives2017.carte.candidat.repositories;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;


@Service
public class CandidatesRepository extends RemoteCsvRepository<Candidate> {

	private DataNormalizer normalizer;
	
	@Autowired
    public CandidatesRepository(DataNormalizer normalizer) throws IOException {
        super("https://framacalc.org/rdseG4rS22.1.csv", "candids.csv");
        this.normalizer = normalizer;
    	this.load(this.url, this.resource);
    }

    @Override
    public Stream<Candidate> all() {
        return super.all()
        		.filter(c -> !"NOPE".equals(c.getSection()));
    }
    
    public Candidate getById(long id) {
        return getUnique(c -> c.getId().equals(id));
    }

    public Candidate getByName(String name) {
        return getUnique(c -> name != null && name.equals(c.getName()));
    }

    public long getMaxId() {
        return this.all()
                .mapToLong(Candidate::getId)
                .max()
                .orElse(0);
    }

    protected Candidate buildElementFrom(String line) {
    	Candidate res = new Candidate(line, normalizer);
    	if (res.getId() == null) {
    		res = null;
    	}
    	return res;
    }
}
