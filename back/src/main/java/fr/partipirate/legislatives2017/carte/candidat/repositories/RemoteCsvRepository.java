package fr.partipirate.legislatives2017.carte.candidat.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class RemoteCsvRepository<T> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected String url;
	protected String resource;
    private List<T> data;

    public RemoteCsvRepository(String url, String resource) throws IOException {
    	this.url = url;
    	this.resource = resource;
    }
    
    public void reload() throws IOException {
    	this.load(this.url, this.resource);
    }
    
    protected void load(String url, String resource) throws IOException {
    	try {
    		this.data = wget(url);
    	} catch (Exception e1) {
    		logger.info("wget failed : "+e1.getMessage());
    		try {
    			this.data = curl(url);
        	} catch (Exception e2) {
        		logger.info("curl failed : "+e2.getMessage());
        		this.data = fallback(resource);
        	}
    	}
    }

    protected abstract T buildElementFrom(String line);

    public long count() {
        return this.data.size();
    }

    public Stream<T> all() {
        return this.data.stream();
    }

    public T getUnique(Predicate<T> filter) {
        return this.all()
                .filter(filter::test)
                .findAny()
                .orElse(null);
    }

	private List<T> fallback(String resource) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(resource), Charset.forName("utf-8")))) {
			return reader.lines().skip(1)
		            .map(this::buildElementFrom)
		            .filter(Objects::nonNull)
		            .collect(Collectors.toList());
		}
	}
	
	private List<T> curl(String url) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("curl", url);
		return stream(pb);
	}

	private List<T> wget(String url) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("wget", "-q", "-O-", url);
		return stream(pb);
	}

	private List<T> stream(ProcessBuilder pb) throws IOException, InterruptedException {
		Process p = pb.start();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), Charset.forName("UTF-8")))) {
			List<T> res = br.lines().skip(1)
		            .map(this::buildElementFrom)
		            .filter(Objects::nonNull)
		            .collect(Collectors.toList());
			int returnCode = p.waitFor();
			if (returnCode != 0) {
				throw new IllegalStateException("Return code is "+returnCode);
			}
			return res;
		}
	}
	
}
