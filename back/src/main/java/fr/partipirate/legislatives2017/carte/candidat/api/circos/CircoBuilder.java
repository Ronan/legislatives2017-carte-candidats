package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.util.List;
import java.util.Objects;

import org.springframework.util.StringUtils;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.beans.Circonscription;

public class CircoBuilder {

	public Circo fromCirconscription(Circonscription circonscription) {
		return new Circo(
				circonscription.getId(),
				circonscription.isOk(), 
				circonscription.getName(), 
				circonscription.getCandidate(),
				circonscription.getSubstitute(),
				circonscription.getLabel(),
				circonscription.getCss(),
				circonscription.getWebsite(),
				circonscription.getPicture());
	}

	public Circo fromCandidates(String circoId, List<Candidate> candidates) {
		Circo res = new Circo(circoId, "");
		for (Candidate candidate : candidates) {
			if (candidate.isCandidate()) {
				res.addCandidat(candidate);
				res.setCss("potentiel");
			}
			if (candidate.isSubstitute()) {
				res.addSuppleant(candidate);
			}
			if (candidate.isRepresentative()) {
				res.addMandataire(candidate);
			}
		}
		long candidatesWithoutSection = candidates.stream()
			.filter(Candidate::isCandidate)
			.map(Candidate::getSection)
			.filter(StringUtils::isEmpty)
			.count();
		if (candidatesWithoutSection > 0) {
			res.setCss("nosection");
		}
		return res;
	}
}
