package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Function;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public abstract class CircoLoader {

	private Path source;
	private Map<String, Circo> data;
	private long lastLoaded;
		
	public Map<String, Circo> load(Path source) throws InvalidFormatException, IOException {
		if (   !source.equals(this.source)
			|| source.toFile().lastModified() > lastLoaded) {
			this.data = loadFromSource(source);
			this.source = source;
			this.lastLoaded = source.toFile().lastModified();
		}
		return this.data;
	}

	protected abstract Map<String, Circo> loadFromSource(Path source) throws IOException, InvalidFormatException;

}
