package fr.partipirate.legislatives2017.carte.candidat.api.labels;

public class Map {

	private String candidat;
	private String candidats;
	private String suppleant;
	private String suppleants;
	private String mandataires;
	private String etiquette;
	private String website;
	private String picture;

	public Map() {
		this.candidat = "Candidat";
		this.candidats = "Candidats";
		this.suppleant = "Suppléant";
		this.suppleants = "Suppléants";
		this.mandataires = "Mandataires";
		this.etiquette = "Etiquette";
		this.website = "Site";
		this.picture = "Photo";
	}

	public String getCandidat() {
		return candidat;
	}

	public String getCandidats() {
		return candidats;
	}

	public String getSuppleant() {
		return suppleant;
	}

	public String getSuppleants() {
		return suppleants;
	}

	public String getMandataires() {
		return mandataires;
	}

	public String getEtiquette() {
		return etiquette;
	}

	public String getWebsite() {
		return website;
	}

	public String getPicture() {
		return picture;
	}

}
