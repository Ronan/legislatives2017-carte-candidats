package fr.partipirate.legislatives2017.carte.candidat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfiguration {
	
	@Value("${data.path}")
	private String dataPath;
	@Value("${display.mode}")
	private String displayMode;

	public String getDataPath() {
		return dataPath;
	}

	public String getDisplayMode() {
		return displayMode;
	}

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	public void setDisplayMode(String displayMode) {
		this.displayMode = displayMode;
	}
	
	
}
