package fr.partipirate.legislatives2017.carte.candidat.repositories;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.beans.Circonscription;

@Service
public class CirconscriptionsRepository extends RemoteCsvRepository<Circonscription> {

    public CirconscriptionsRepository() throws IOException {
        super("https://framacalc.org/jV6Bul43fE.csv", "circos.csv");
    	this.load(this.url, this.resource);
    }

    protected Circonscription buildElementFrom(String line) {
        return new Circonscription(line);
    }
    
    public Stream<Circonscription> allNonEmpty() {
        return this.all()
                .filter(Circonscription::isOk);
    }

    public Circonscription getById(String id) {
        return this.getUnique(c -> id.equals(c.getId()));
    }

    public Stream<Candidate> allCandidates() {
        return allNonEmpty()
                .flatMap(circo -> Stream.of(
                        new Candidate(","+circo.getCandidate()+",,1,0,0,,,,#"+circo.getId()),
                        new Candidate(","+circo.getSubstitute()+",,0,1,0,,,,#"+circo.getId())
                        )
                    )
                .filter(candidate -> !"".equals(candidate.getName().trim()));
    }

}
