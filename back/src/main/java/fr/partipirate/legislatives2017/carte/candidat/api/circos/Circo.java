package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.util.List;
import java.util.ArrayList;
import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;

public class Circo {

	private String id;
	private boolean ok;
	private String nom;
	private String candidat;
	private String suppleant;
	private String etiquette;
	private String css;
	private String website;
	private String picture;
	private List<Candidat> candidats = new ArrayList<>();
	private List<Candidat> suppleants = new ArrayList<>();
	private List<Candidat> mandataires = new ArrayList<>();
	
	public Circo(String id, boolean ok, String nom, String candidat, String suppleant, String etiquette, String css, String website,
			String picture) {
		super();
		this.id = id;
		this.ok = ok;
		this.nom = nom;
		this.candidat = candidat;
		this.suppleant = suppleant;
		this.etiquette = etiquette;
		this.css = css;
		this.website = website;
		this.picture = picture;
	}

	public Circo(String title, String desc) {
		super();
		this.id = title;
		this.ok = false;
		this.nom = title;
		this.candidat = "-";
		this.suppleant = "-";
		this.etiquette = "-";
		this.css = "";
		this.website = "-";
		this.picture = "-";
	}

	public String getId() {
		return id;
	}
	
	public boolean isOk() {
		return ok;
	}

	public String getNom() {
		return nom;
	}

	public String getCandidat() {
		return candidat;
	}

	public String getSuppleant() {
		return suppleant;
	}

	public String getEtiquette() {
		return etiquette;
	}

	public String getCss() {
		return css;
	}

	public String getWebsite() {
		return website;
	}

	public String getPicture() {
		return picture;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public void setCandidat(String candidat) {
		this.candidat = candidat;
	}

	public void setSuppleant(String suppleant) {
		this.suppleant = suppleant;
	}

	public void setEtiquette(String etiquette) {
		this.etiquette = etiquette;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public void addCandidat(Candidate candidate) {
		this.candidats.add(new Candidat(candidate.getName(), candidate.getCircos()));
	}

	public void addSuppleant(Candidate candidate) {
		this.suppleants.add(new Candidat(candidate.getName(), candidate.getCircos()));
	}

	public void addMandataire(Candidate candidate) {
		this.mandataires.add(new Candidat(candidate.getName(), candidate.getCircos()));
	}

	public List<Candidat> getCandidats() {
		return candidats;
	}

	public List<Candidat> getSuppleants() {
		return suppleants;
	}

	public List<Candidat> getMandataires() {
		return mandataires;
	}
	
	
}
