package fr.partipirate.legislatives2017.carte.candidat.api.reload;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/reload")
public class ReloadController {

	private CandidatesRepository candidates;
	private CirconscriptionsRepository circonscriptions;

	@Autowired
    public ReloadController(CandidatesRepository candidates, CirconscriptionsRepository circonscriptions) {
		this.candidates = candidates;
		this.circonscriptions = circonscriptions;
	}

	@RequestMapping(method= RequestMethod.GET)
	public @ResponseBody Status stats() throws IOException {
		this.candidates.reload();
		this.circonscriptions.reload();
		return new Status("OK");
	}

}
