package fr.partipirate.legislatives2017.carte.candidat.api.labels;

public class Labels {

    private String error;
    private Login login;
    private About about;
    private Map map;
    private Stats stats;

    public Labels() {
        this.error = "Error";
        this.login = new Login();
        this.about = new About();
        this.map = new Map();
        this.stats = new Stats();
    }

    public String getError() {
        return error;
    }

    public Login getLogin() {
        return login;
    }

    public About getAbout() {
        return about;
    }

    public Map getMap() {
        return map;
    }

	public Stats getStats() {
		return stats;
	}

}
