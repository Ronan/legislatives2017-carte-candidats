package fr.partipirate.legislatives2017.carte.candidat.api.login;

public class Authentification {

	private String auth;

	public Authentification(String auth) {
		super();
		this.auth = auth;
	}

	public String getAuth() {
		return auth;
	}
}
