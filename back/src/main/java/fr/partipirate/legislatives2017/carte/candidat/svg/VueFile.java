package fr.partipirate.legislatives2017.carte.candidat.svg;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VueFile {

	private List<String> lines;

	public VueFile(List<String> lines) {
		this.lines = lines;
	}

	public HtmlFragment getTemplate() {
		int start = lines.indexOf("<template>");
		int end = lines.indexOf("</template>");
		return new HtmlFragment(this.lines.subList(start+1, end));
	}

	public VueFile setTemplate(HtmlFragment html) {
		int start = lines.indexOf("<template>");
		int end = lines.indexOf("</template>");
		
		List<String> newLines = new ArrayList<>();
		newLines.addAll(lines.subList(0, start));
		newLines.add("<template>");
		newLines.add("  <div>");
		newLines.addAll(html.getAllBlocks("svg").get(0).getLines());
		newLines.add("  </div>");
		newLines.add("</template>");
		newLines.addAll(lines.subList(end, lines.size()-1));
		
		return new VueFile(newLines);
	}
	
	public String toString() {
		return lines.stream().collect(Collectors.joining("\n"));
	}

}
