package fr.partipirate.legislatives2017.carte.candidat;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.HashMap;
import java.util.Map;

@EnableWebMvc
@SpringBootApplication
public class App {

    public static void main(String...args) throws Exception {
        new SpringApplicationBuilder()
                .sources(App.class)
                .run(args);
    }

}
