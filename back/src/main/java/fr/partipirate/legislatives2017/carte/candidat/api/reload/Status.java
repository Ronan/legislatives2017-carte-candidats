package fr.partipirate.legislatives2017.carte.candidat.api.reload;

public class Status {

	private String result;

	public Status(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}
	
}
