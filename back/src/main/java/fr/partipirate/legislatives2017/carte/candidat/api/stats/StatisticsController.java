package fr.partipirate.legislatives2017.carte.candidat.api.stats;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/stats")
public class StatisticsController {

	private CandidatesRepository repository;

	@Autowired
    public StatisticsController(CandidatesRepository repository) {
		this.repository = repository;
	}

	@RequestMapping(method= RequestMethod.GET)
	public @ResponseBody Map<String, String> stats() {
    	Map<String, String> res = new HashMap<>();
    	res.put("repository.size", String.valueOf(repository.count()));
    	res.put("repository.maxid", String.valueOf(repository.getMaxId()));
		return res;
	}

}
