package fr.partipirate.legislatives2017.carte.candidat.api.login;

public class Credential {

	private String login;
	private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
