package fr.partipirate.legislatives2017.carte.candidat.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.partipirate.legislatives2017.carte.candidat.beans.Circonscription;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;

@Service
public class DataNormalizer {

	private CirconscriptionsRepository circonscriptions;
	
	@Autowired
	public DataNormalizer(CirconscriptionsRepository circonscriptions) {
		this.circonscriptions = circonscriptions;
	}
	
	public List<String> normalize(String data) {
		List<String> res = new ArrayList<>();
		if (data != null) {
			if ("#Circonscription 11 des francais a l'etranger".equals(data)) {
				data = "999-11";
			}
			res = Arrays.stream(data.split(" |,|\\."))
					.map(String::trim)
					.map(String::toLowerCase)
					.map(s -> s.replace("#circoparis", "#circo75"))
					.map(s -> s.replace("#paris", "#circo75"))
					.map(s -> s.replace("#circoyvelines", "#circo78"))
					.map(s -> s.replace("#circo33bordeaux01", "#circo3301"))
					.map(s -> s.replace("#bagnolet93170", "#circo9307"))
					.map(s -> s.replace("villeurbanne", "#circo6906"))
					.map(s -> s.replace("lilas", "#circo9309"))
					.flatMap(this::normalizeToken)
					.distinct()
					.map(s -> s.replace("099", "999"))
					.collect(Collectors.toList());
		}
		return res;
	}
	
	protected Stream<String> normalizeToken(String data) {
		Stream<String> res = Stream.of();
		for (CircosPattern pattern : CircosPattern.values()) {
			res = Stream.concat(res, pattern.match(circonscriptions, data));
		}
		return res;
	}

	public enum CircosPattern {
		NOMINAL("#circo([0-9][0-9ab])([0-9][0-9])"),
		NOMINAL0("#circo([0-9][0-9])0([0-9][0-9])"),
		NOHASH("circo([0-9][0-9])([0-9][0-9])"),
		NOCIRCO("#([0-9][0-9])([0-9][0-9])"),
		NOCIRCONOHASH("([0-9][0-9])([0-9][0-9])"),
		NOCIRCODASHED("([0-9][0-9])-([0-9][0-9])"),
		NOCIRCODASHED3("0([0-9][0-9])-([0-9][0-9])"),
		NOCIRCODASHEDHASH("#0([0-9][0-9])-([0-9][0-9])"),
		NOCIRCO0("#([0-9][0-9])0([0-9][0-9])"),
		ETRANGER("#circofretr([0-9][0-9])") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return Stream.of("999-"+matcher.group(1));
				} else {
					return Stream.of();
				}
			}
		},
		ETRANGER0("#circofretr([0-9])") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return Stream.of("999-0"+matcher.group(1));
				} else {
					return Stream.of();
				}
			}
		},
		ETRANGERDASHED("999-([0-9][0-9])") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return Stream.of("999-"+matcher.group(1));
				} else {
					return Stream.of();
				}
			}
		},
		ETRANGERALL("#circofretr") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return circos.all()
						.map(Circonscription::getId)
						.filter(id -> id.startsWith("999"));
				} else {
					return Stream.of();
				}
			}
		},
		DEPARTEMENT("#circo([0-9][0-9])") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					String dept = matcher.group(1);
					return circos.all()
						.map(Circonscription::getId)
						.filter(id -> id.startsWith("0"+dept));
				} else {
					return Stream.of();
				}
			}
		},
		DEPARTEMENTNOHASH("circo([0-9][0-9])") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					String dept = matcher.group(1);
					return circos.all()
						.map(Circonscription::getId)
						.filter(id -> id.startsWith("0"+dept));
				} else {
					return Stream.of();
				}
			}
		},
		GUADELOUPE("#circo97[0-9][0-9][0-9]") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return circos.all()
						.map(Circonscription::getId)
						.filter(id -> id.startsWith("971"));
				} else {
					return Stream.of();
				}
			}
		},
		LYON("lyon") {
			@Override
			public Stream<String> match(CirconscriptionsRepository circos, String data) {
				Matcher matcher = pattern.matcher(data);
				if (matcher.matches()) {
					return Stream.of("069-01", "069-02", "069-03", "069-04");
				} else {
					return Stream.of();
				}
			}
		},
		;
		
		protected Pattern pattern;
		
		private CircosPattern(String regex) {
			this.pattern = Pattern.compile(regex);
		}
		
		public Stream<String> match(CirconscriptionsRepository circos, String data) {
			Matcher matcher = pattern.matcher(data);
			if (matcher.matches()) {
				return Stream.of("0"+matcher.group(1)+"-"+matcher.group(2));
			} else {
				return Stream.of();
			}
		}
	}
}
