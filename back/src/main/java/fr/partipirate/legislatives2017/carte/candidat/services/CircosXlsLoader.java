package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircosXlsLoader extends CircoLoader {

	@Override
	protected Map<String, Circo> loadFromSource(Path xls) throws IOException, InvalidFormatException{
    	Map<String, Circo> res = new HashMap<>();
    	try (Workbook wb = new XSSFWorkbook(xls.toFile());) {
    		 Sheet sheet = wb.getSheetAt(0);
    		 for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
    			 Row row = sheet.getRow(rowIndex);
    			 if ("true".equalsIgnoreCase(getStringCellValue(row, 2))) {
        			 res.put(row.getCell(0).getStringCellValue(),
        					 new Circo(
        							 getStringCellValue(row, 0),
        							 true,
        							 getStringCellValue(row, 1),
        							 getStringCellValue(row, 3),
        							 getStringCellValue(row, 4),
        							 getStringCellValue(row, 5),
        							 getStringCellValue(row, 6),
        							 getStringCellValue(row, 7),
        							 getStringCellValue(row, 8)
        							 )
        					 );
    			 }
    		 }
    	} 
		return res;
	}
	
	private String getStringCellValue(Row row, int index) {
		DataFormatter formatter = new DataFormatter();
		return Optional.ofNullable(row.getCell(index))
			.map(cell -> formatter.formatCellValue(cell))
			.orElse("");
	}
}
