package fr.partipirate.legislatives2017.carte.candidat.svg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlFragment {

	private List<String> lines;

	public HtmlFragment(List<String> lines) {
		this.lines = lines;
	}

	public HtmlFragment(List<String> lines, String block) throws IOException {
	    String data = lines.stream().collect(Collectors.joining("\n"));
	    data = data.substring(data.indexOf("<"+block));
	    data = data.substring(0, data.indexOf("</"+block+">"));
	    data = "<div>\n" + data + "</"+block+">\n</div>";
	    this.lines = Arrays.asList(data.split("\n"));
	}
	
	public HtmlFragment add(String name, String line) {
		int markStart = this.lines.stream()
				.map(String::trim)
				.collect(Collectors.toList()).indexOf("<"+name);

		List<String> newLines = new ArrayList<>();
		newLines.addAll(lines.subList(0, markStart+1));
		newLines.add(line);
		newLines.addAll(lines.subList(markStart+1, lines.size()-1));
		return new HtmlFragment(newLines);
	}
	
	public HtmlFragment addBind(String name, String field, String line) {
		int index = 0;
		List<String> newLines = new ArrayList<>();
		while (index < this.lines.size()) {
			newLines.add(this.lines.get(index));
			if (this.lines.get(index).contains("<path")) {
				int subIndex = index;
				while (!this.lines.get(subIndex).contains("id=\"") && subIndex < this.lines.size()) {
					subIndex++;
				}
				String idLine = this.lines.get(subIndex);
				String id = idLine.substring(idLine.indexOf("id=\"")+4);
				id = id.substring(0, id.indexOf("\""));
				if (id.matches("[0-9][0-9][0-9ab]-[0-9][0-9]") && !id.matches("[0-9][0-9][0-9ab]-00")) {
					newLines.add(line.replace("$VALUE", id));
				}
			}
			index++;
		}
		return new HtmlFragment(newLines);
	}

	public HtmlFragment removeLines(String toRemove) {
		List<String> newLines = this.lines.stream()
			.filter(current -> !current.equals(toRemove))
			.collect(Collectors.toList());
		return new HtmlFragment(newLines);
	}

	public String firstLine() {
		return lines.get(0).trim();
	}

	public String lastLine() {
		return lines.get(lines.size()-1).trim();
	}

	public List<String> getFirstMark(String name) {
		int markStart = this.lines.stream()
				.map(String::trim)
				.collect(Collectors.toList()).indexOf("<"+name);		
		int markEnd = markStart;
		while (!this.lines.get(markEnd).contains(">")) {
			markEnd++;
		}
		
		return this.lines.subList(markStart, markEnd);
	}

	public List<HtmlBlock> getAllBlocks(String name) {
		String data = lines.stream()
			.collect(Collectors.joining("\n"));
		data = data.substring(data.indexOf("<"+name));
		return Arrays.stream(data.split("<"+name))
			.filter(s -> s.contains("</"+name+">"))
			.map(s -> s.substring(0, s.indexOf("</"+name+">")))
			.map(s -> "<"+name+s+"</"+name+">")
			.map(s -> new HtmlBlock(s))
			.collect(Collectors.toList());
	}

}
