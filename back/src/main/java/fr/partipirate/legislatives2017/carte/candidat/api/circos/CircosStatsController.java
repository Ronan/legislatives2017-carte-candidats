package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/circo")
public class CircosStatsController {

	@RequestMapping(value="/with-non-registered", method= RequestMethod.GET)
	public @ResponseBody List<String> all() {
		return null;
	}

}
