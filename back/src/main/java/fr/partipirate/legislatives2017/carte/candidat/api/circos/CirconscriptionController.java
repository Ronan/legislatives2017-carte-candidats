package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.carte.candidat.AppConfiguration;
import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.beans.Circonscription;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/circos")
public class CirconscriptionController {

	private CandidatesRepository candidates;
	private CirconscriptionsRepository circonscriptions;
	private DataNormalizer normalizer;
	private AppConfiguration config;
	
	@Autowired
	public CirconscriptionController(
			CandidatesRepository candidates, 
			CirconscriptionsRepository circonscriptions,
			DataNormalizer normalizer,
			AppConfiguration config
			) {
		this.candidates = candidates;
		this.circonscriptions = circonscriptions;
		this.normalizer = normalizer;
		this.config = config;
	}

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody Map<String, Circo> getCircos() throws IOException, InvalidFormatException {
    	Map<String, Circo> res;
    	if ("public".equals(config.getDisplayMode())) {
    		res = getPublicCircos();
    	} else {
    		res = getPotentialCircos();
    	}
    	return res;
    }

    public Map<String, Circo> getPublicCircos() throws IOException, InvalidFormatException {
    	return circonscriptions.all()
        		.map(circo -> new CircoBuilder().fromCirconscription(circo))
        		.collect(Collectors.toMap(Circo::getId, Function.identity()));
    }
    
    public Map<String, Circo> getPotentialCircos() throws IOException, InvalidFormatException {
    	Map<String, Circo> res = new TreeMap<>(allCircoWithPotentialCandidates().entrySet().stream()
	    		.collect(Collectors.toMap(
	    				Map.Entry::getKey,
	    				entry -> new CircoBuilder().fromCandidates(entry.getKey(), entry.getValue()))));
    	
    	Map<String, Circo> invest = circonscriptions.allNonEmpty()
        		.map(circo -> new CircoBuilder().fromCirconscription(circo))
        		.collect(Collectors.toMap(Circo::getId, Function.identity()));
        	
    	res.putAll(invest);
        		
    	return res;
    }

    
	@RequestMapping(value="/potential", method= RequestMethod.GET)
	public @ResponseBody Map<String, List<Candidate>> allCircoWithPotentialCandidates() {
		return circonscriptions.all()
			.map(Circonscription::getId)
			.collect(Collectors.toMap(Function.identity(), this::listPotentialCandidates));
	}
	
	@RequestMapping(value="/{circo}/potential", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> listPotentialCandidates(@PathVariable(value="circo") String circo) {
		Set<String> investis = circonscriptions.allCandidates()
				.map(Candidate::getName)
				.collect(Collectors.toSet());
		
		return candidates.all()
			.filter(c -> c.getNormalcircos().contains(circo))
			.filter(c -> !investis.contains(c.getName()))
			.filter(Objects::nonNull)
			.distinct()
			.collect(Collectors.toList());
	}
	
	public Stream<Candidate> fromCirconscriptions(String circo) {
		List<Candidate> res = new ArrayList<>();
		Circonscription circonscription = circonscriptions.getById(circo);
		if (circonscription != null) {
			Candidate candidate = candidates.getByName(circonscription.getCandidate());
			if (candidate != null) {
				res.add(candidate);
			}
			Candidate substitute = candidates.getByName(circonscription.getSubstitute());
			if (substitute != null){
				res.add(substitute);
			}
		}
		return res.stream();
	}
	
	public Stream<Candidate> fromCandidates(String circo) {
		return candidates.all()
				.filter(c -> normalizer.normalize(c.getCircos()).contains(circo));
	}
}
