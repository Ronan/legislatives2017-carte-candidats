package fr.partipirate.legislatives2017.carte.candidat.api.labels;

public class Stats {
    private String title = "Statistiques";
    private String nocirco = "Candidats sans circonscriptions";
    private String reload = "Reload";

    public String getTitle() {
		return title;
	}
	public String getNocirco() {
		return nocirco;
	}
	public String getReload() {
		return reload;
	}
	
}
