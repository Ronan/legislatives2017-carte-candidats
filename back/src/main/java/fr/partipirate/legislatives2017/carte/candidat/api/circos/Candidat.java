package fr.partipirate.legislatives2017.carte.candidat.api.circos;

public class Candidat {

	private String nom;
	private String circos;

	public Candidat(String nom, String circos) {
		this.nom = nom;
		this.circos = circos;
	}

	public String getNom() {
		return nom;
	}

	public String getCircos() {
		return circos;
	}
}
