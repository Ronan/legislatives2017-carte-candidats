package fr.partipirate.legislatives2017.carte.candidat.api.candidate;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/candidates")
public class CandidatesController {

	private CandidatesRepository candidates;
	private CirconscriptionsRepository circonscriptions;
	private DataNormalizer normalizer;


	@Autowired
    public CandidatesController(
    		CandidatesRepository candidates, 
    		CirconscriptionsRepository circonscriptions,
    		DataNormalizer normalizer) {
		this.candidates = candidates;
		this.circonscriptions = circonscriptions;
		this.normalizer = normalizer;
	}
	
	@RequestMapping(value="/without-circo", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> withoutCirco() {
		Set<String> investis = circonscriptions.allCandidates()
				.map(Candidate::getName)
				.collect(Collectors.toSet());

		return candidates.all()
				.filter(c -> normalizer.normalize(c.getCircos()).size() == 0)
				.filter(c -> !investis.contains(c.getName()))
				.collect(Collectors.toList());
	}

	@RequestMapping(value="/not-registered", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> notRegistered() {
		return circonscriptions.allCandidates()
				.filter(candidate -> candidates.getByName(candidate.getName()) == null)
				.collect(Collectors.toList());
	}

	@RequestMapping(value="/without-section", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> withoutSection() {
		return candidates.all()
				.filter(c -> c.getSection() != null)
				.filter(candidate -> "".equals(candidate.getSection().trim()))
				.collect(Collectors.toList());
	}
}
