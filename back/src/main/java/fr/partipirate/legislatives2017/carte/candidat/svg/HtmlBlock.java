package fr.partipirate.legislatives2017.carte.candidat.svg;

import java.util.Arrays;
import java.util.List;

public class HtmlBlock {

	private String data;
	
	public HtmlBlock(String data) {
		this.data = data;
	}
	
	public String getId() {
		String res = data;
		res = res.substring(res.indexOf("id=\"")+4);
		res = res.substring(0, res.indexOf("\""));
		return res;
	}
	
	public String getValue(String name) {
		String res = data.substring(data.indexOf("<"+name), data.indexOf("</"+name+">"));
		res = res.substring(res.indexOf(">")+1);
		return res;
	}

	public boolean hasBindedClass() {
		return data.contains("class=\"circo {{ cssCirco('"+this.getId()+"') }}\"");
	}
	
	public List<String> getLines() {
		return Arrays.asList(data.split("\n"));
	}
	
	public String toString() {
		return this.data;
	}

}
