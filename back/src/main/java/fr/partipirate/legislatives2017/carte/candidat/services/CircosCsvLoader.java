package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircosCsvLoader extends CircoLoader {

	@Override
	protected Map<String, Circo> loadFromSource(Path csv) throws IOException {
		return loadFromSource(Files.lines(csv));
	}

	protected Map<String, Circo> loadFromSource(Stream<String> lines) {
    	return lines
    			.map(l -> l.split(";"))
        		.filter(line -> "true".equals(line[2]))
        		.map(line -> new ImmutablePair<String, Circo>(
        							line[0], 
        							new Circo(line[0], true, line[1], line[3], line[4], line[5], line[6], line[7], line[8])
        							))
        		.collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
	}

}
