package fr.partipirate.legislatives2017.carte.candidat.api.sections;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/sections")
public class SectionsController {

	private CandidatesRepository candidates;
	
	@Autowired
    public SectionsController(CandidatesRepository candidates) {
		this.candidates = candidates;
	}

	@RequestMapping(value="/all", method= RequestMethod.GET)
	public @ResponseBody List<String> all() {
		return candidates.all()
			.map(candidate -> candidate.getSection())
			.filter(section -> section != null)
			.map(String::trim)
			.filter(section -> !"".equals(section))
			.filter(section -> !"NOPE".equals(section))
			.distinct()
			.collect(Collectors.toList());
	}

	@RequestMapping(value="/{name}/candidates", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> candidates(@PathVariable(value="name") String name) {
		return candidates.all()
			.filter(candidate -> name.equals(candidate.getSection()))
			.collect(Collectors.toList());
	}

	@RequestMapping(value="/{name}/no-tutor", method= RequestMethod.GET)
	public @ResponseBody List<Candidate> withoutTutor(@PathVariable(value="name") String name) {
		return candidates.all()
				.filter(candidate -> name.equals(candidate.getSection()))
				.filter(candidate -> candidate.getTutor() == null || "".equals(candidate.getTutor().trim()))
				.collect(Collectors.toList());
	}
}
