package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.partipirate.legislatives2017.carte.candidat.AppConfiguration;
import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;
import fr.partipirate.legislatives2017.carte.candidat.svg.HtmlBlock;
import fr.partipirate.legislatives2017.carte.candidat.svg.HtmlFragment;

@Service
public class CircosService {

	private static final String RESOURCES_CARTE_HTML = "/Carte.html";
	
	private CircosCsvLoader csvLoader = new CircosCsvLoader();
	private CircosXlsLoader xlsLoader = new CircosXlsLoader();
	
	private String root;
	
	public CircosService(String root) {
		this.root = root;
	}
	
	@Autowired
	public CircosService(AppConfiguration config) {
		this(config.getDataPath());
	}
	
	public Map<String, Circo> getAllCircos() throws IOException, InvalidFormatException {
    	Map<String, Circo> res = this.getCircosFromHtmlTemplate();

    	Path xls = Paths.get(root+"/circos.xlsx");
    	if (xls.toFile().exists()) {
    		xlsLoader.load(xls).entrySet().stream()
				.forEach(entry -> res.put(entry.getKey(), entry.getValue()));
    	} else {
	    	Path csv = Paths.get(root+"/circos.csv");
	    	if (csv.toFile().exists()) {
	    		csvLoader.load(csv).entrySet().stream()
	    			.forEach(entry -> res.put(entry.getKey(), entry.getValue()));
	    	}
    	}
    	return res;
	}
	
	protected Map<String, Circo> getCircosFromHtmlTemplate() throws IOException {
	    HtmlFragment template = new HtmlFragment(getLinesFromFile(RESOURCES_CARTE_HTML), "svg");
		
		List<HtmlBlock> circos = template.getAllBlocks("path");
		
		return circos.stream()
			.collect(Collectors.toConcurrentMap(
					block -> block.getId(),
					block -> new Circo(block.getValue("title"), block.getValue("desc")))
					);
    }
    
	private List<String> getLinesFromFile(String map) throws IOException {
	    try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(map), StandardCharsets.UTF_8))) {
	        return reader.lines().collect(Collectors.toList());
	    }
	}
	
}
