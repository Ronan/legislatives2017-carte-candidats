package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;

public class CircoBuilderShould {

	@Test
	public void build_empty_circo_when_no_candidate() {
		Circo circo = new CircoBuilder().fromCandidates("075-01", new ArrayList<>());
		Assert.assertFalse(circo.isOk());
	}

	@Test
	public void build_circo_when_potential_candidate() {
		List<Candidate> candidates = new ArrayList<>();
		candidates.add(new Candidate("42,John Doe,male,1,1,1"));
		Circo circo = new CircoBuilder().fromCandidates("075-01", candidates);
		Assert.assertFalse(circo.isOk());
		Assert.assertEquals("nosection", circo.getCss());
	}

	@Test
	public void build_circo_when_potential_candidate_has_section() {
		List<Candidate> candidates = new ArrayList<>();
		candidates.add(new Candidate("42,John Doe,male,1,1,1,IdF"));
		Circo circo = new CircoBuilder().fromCandidates("075-01", candidates);
		Assert.assertFalse(circo.isOk());
		Assert.assertEquals("potentiel", circo.getCss());
	}

	@Test
	public void build_circo_when_no_potential_candidates() {
		List<Candidate> candidates = new ArrayList<>();
		candidates.add(new Candidate("42,John Doe,male,0,1,1"));
		Circo circo = new CircoBuilder().fromCandidates("075-01", candidates);
		Assert.assertFalse(circo.isOk());
		Assert.assertEquals("", circo.getCss());
	}
}
