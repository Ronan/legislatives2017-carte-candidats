package fr.partipirate.legislatives2017.carte.candidat.api.candidate;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class CandidatesControllerShould {
	
	private static CandidatesController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circonscriptions = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circonscriptions);
		controller = new CandidatesController(
				new CandidatesRepository(normalizer), 
				circonscriptions,
				normalizer);
	}
	
	@Test
	public void not_provide_registered_candidates() {
		List<Candidate> notRegistered = controller.notRegistered();
		long count = notRegistered.stream()
				.filter(candidate -> "Ronan Le Roy".equals(candidate.getName()))
				.count();
		Assert.assertEquals(0l, count);
	}

	@Test
	public void filter_candidates_without_circo() {
		List<Candidate> withoutCirco = controller.withoutCirco();
		Assert.assertEquals(0l, withoutCirco.stream().filter(c -> "Ronan Le Roy".equals(c.getName())).count());
		Assert.assertEquals(1l, withoutCirco.stream().filter(c -> "Sylvain De Smet".equals(c.getName())).count());
	}
}
