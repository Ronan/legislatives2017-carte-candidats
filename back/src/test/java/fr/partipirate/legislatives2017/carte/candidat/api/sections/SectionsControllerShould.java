package fr.partipirate.legislatives2017.carte.candidat.api.sections;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.api.sections.SectionsController;
import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class SectionsControllerShould {
	
	private static SectionsController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circos = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circos);
		CandidatesRepository repository = new CandidatesRepository(normalizer);

		controller = new SectionsController(repository);
	}
	

	@Test
	public void list_all_sections() {
		List<String> sections = controller.all();
		Assert.assertEquals(7, sections.size());
		Assert.assertTrue(sections.contains("IdF"));
	}
	
	@Test
	public void list_all_candidates_without_tuteur_by_sections() {
		List<Candidate> candidates = controller.withoutTutor("IdF");
		Assert.assertEquals(15, candidates.size());
		Assert.assertEquals(0l, candidates.stream().filter(c -> "Ronan Le Roy".equals(c.getName())).count());
		Assert.assertEquals(1l, candidates.stream().filter(c -> "Thomas Watanabe-Vermorel".equals(c.getName())).count());
	}
}
