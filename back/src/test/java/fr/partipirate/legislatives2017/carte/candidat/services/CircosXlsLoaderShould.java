package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircosXlsLoaderShould {

	@Test
	public void load_from_xls() throws InvalidFormatException, IOException {
		CircosXlsLoader loader = new CircosXlsLoader();
		
		Map<String, Circo> circos = loader.load(Paths.get("./data/circos.xlsx"));

		Assert.assertThat(circos.size(), Matchers.is(15));
		Assert.assertTrue(circos.containsKey("075-01"));
		Assert.assertThat(circos.get("075-01").getNom(), Matchers.is("Première circonscription de Paris"));
		Assert.assertThat(circos.get("075-01").getCandidat(), Matchers.is("Ronan Le Roy"));
		Assert.assertThat(circos.get("075-01").getSuppleant(), Matchers.is("Sandrine Villers"));
		Assert.assertTrue(circos.get("075-01").isOk());
		Assert.assertThat(circos.get("075-01").getEtiquette(), Matchers.is("Parti Pirate"));
		Assert.assertThat(circos.get("075-01").getCss(), Matchers.is("pirate"));
		Assert.assertThat(circos.get("075-01").getWebsite(), Matchers.is("https://twitter.com/ronanleroy"));
		Assert.assertThat(circos.get("075-01").getPicture(), Matchers.is("https://pbs.twimg.com/profile_images/1083709532/avatar.jpg"));
	}
}
