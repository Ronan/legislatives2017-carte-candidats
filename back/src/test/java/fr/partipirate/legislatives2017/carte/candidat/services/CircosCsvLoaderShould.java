package fr.partipirate.legislatives2017.carte.candidat.services;

import java.util.Arrays;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircosCsvLoaderShould {

	@Test
	public void load_circos_from_csv() {
		
		String[] lines = {
				"Numéro;Nom;OK;Candidat;Suppleant;Etiquette;CSS;Website;Picture",
				"001-01;Première circonscription de l'Ain;false;;;;;;",
				"999-01;Première circonscription des français de l'étranger;true;Donald Trump;Hilary Clinton;Politicard;;https://www.election.us/;https://www.election.us/usa.png",
				
		};
		
		CircosCsvLoader loader = new CircosCsvLoader();

		Map<String, Circo> circos = loader.loadFromSource(Arrays.stream(lines));
		Assert.assertThat(circos.size(), Matchers.is(1));
		Assert.assertTrue(circos.containsKey("999-01"));
		Assert.assertThat(circos.get("999-01").getNom(), Matchers.is("Première circonscription des français de l'étranger"));
		Assert.assertThat(circos.get("999-01").getCandidat(), Matchers.is("Donald Trump"));
		Assert.assertThat(circos.get("999-01").getSuppleant(), Matchers.is("Hilary Clinton"));
		Assert.assertTrue(circos.get("999-01").isOk());
		Assert.assertThat(circos.get("999-01").getEtiquette(), Matchers.is("Politicard"));
		Assert.assertThat(circos.get("999-01").getWebsite(), Matchers.is("https://www.election.us/"));
		Assert.assertThat(circos.get("999-01").getPicture(), Matchers.is("https://www.election.us/usa.png"));

	}
}
