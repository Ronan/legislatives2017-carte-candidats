package fr.partipirate.legislatives2017.carte.candidat.repositories;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.beans.Circonscription;

public class CirconscriptionsRepositoryShould {

    private static CirconscriptionsRepository repository;

    @BeforeClass
    public static void setup() throws IOException {
        repository = new CirconscriptionsRepository();
    }

    @Test
    public void retreive_577_circonscriptions() {
        Assert.assertEquals(577, repository.count());
    }

    @Test
    public void retreive_a_circo_with_id_001_01() {
        Circonscription expected = new Circonscription("001-01,Première circonscription de l'Ain,0");
        Assert.assertEquals(expected, repository.getById("001-01"));
    }

    @Test
    public void retreive_a_circo_with_id_075_01() {
        Circonscription expected = new Circonscription("075-01,Première circonscription de Paris,1,LE ROY Ronan,Sandrine Villers,Parti Pirate,pirate,https://twitter.com/ronanleroy,https://pbs.twimg.com/profile_images/1083709532/avatar.jpg");
        Assert.assertEquals(expected, repository.getById("075-01"));
    }
    
    @Test
    public void retreive_a_circo_with_id_02b_02() {
        Circonscription expected = new Circonscription("02b-02,Deuxième circonscription de Haute-Corse,0");
        Assert.assertEquals(expected, repository.getById("02b-02"));
    }


    @Test
    public void have_11_circo_ok() {
        Assert.assertEquals(101, repository.allNonEmpty().count());
        Assert.assertTrue(repository.getById("092-04").isOk());
        Assert.assertTrue(repository.getById("093-05").isOk());
        Assert.assertTrue(repository.getById("092-08").isOk());
        Assert.assertTrue(repository.getById("075-18").isOk());
        Assert.assertTrue(repository.getById("092-12").isOk());
        Assert.assertTrue(repository.getById("075-15").isOk());
        Assert.assertTrue(repository.getById("093-07").isOk());
        Assert.assertTrue(repository.getById("092-11").isOk());
        Assert.assertTrue(repository.getById("075-01").isOk());
        Assert.assertTrue(repository.getById("075-05").isOk());
        Assert.assertTrue(repository.getById("075-06").isOk());
    }
}
