package fr.partipirate.legislatives2017.carte.candidat.api.stats;

import java.io.IOException;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.api.stats.StatisticsController;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class StatisticsControllerShould {

	private static StatisticsController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circos = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circos);
		CandidatesRepository repository = new CandidatesRepository(normalizer);

		controller = new StatisticsController(repository);
	}
	
	@Test
	public void provide_repository_size() {
		Map<String, String> stats = controller.stats();
		Assert.assertEquals("247", stats.get("repository.size"));
	}
	
	@Test
	public void provide_repository_max_id() {
		Map<String, String> stats = controller.stats();
		Assert.assertEquals("277", stats.get("repository.maxid"));
	}

}
