package fr.partipirate.legislatives2017.carte.candidat.svg;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class MapSvgVueShould {

	private static final int NB_OF_CIRCO = 577;

	private static final String FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE = "../front/src/components/map/map-svg.vue";
	private static final String BACK_SRC_MAIN_RESOURCES_CARTE_HTML = "src/main/resources/Carte.html";
	private static final String BACK_SRC_TEST_RESOURCES_MAP_SVG_VUE = "src/test/resources/map-svg.vue";
	
	@Test
	public void be_enclosed_in_a_single_div() throws IOException {
		VueFile vue = new VueFile(getLinesFromFile(FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE));
		HtmlFragment template = vue.getTemplate();
		Assert.assertThat(template.firstLine(), Matchers.is("<div class=\"map\">"));
		Assert.assertThat(template.lastLine(), Matchers.is("</div>"));
	}

	@Test
	public void have_a_clicked_listener_on_enclosing_g() throws IOException {
		VueFile vue = new VueFile(getLinesFromFile(FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE));
		HtmlFragment template = vue.getTemplate();
		
		List<String> gMark = template.getFirstMark("g");

		boolean found = gMark.stream()
			.map(String::trim)
			.filter(s -> "@click=\"clicked\"".equals(s))
			.findFirst()
			.map(s -> true)
			.orElse(false);
		
		Assert.assertTrue(found);
	}
	
	@Test
	public void count_577_circo() throws IOException {
		VueFile vue = new VueFile(getLinesFromFile(FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE));
		HtmlFragment template = vue.getTemplate();

		List<HtmlBlock> circos = template.getAllBlocks("path");
		
		Assert.assertThat(circos.size(), Matchers.is(NB_OF_CIRCO));
	}

	@Test
	public void have_binded_class() throws IOException {
		VueFile vue = new VueFile(getLinesFromFile(FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE));
		HtmlFragment template = vue.getTemplate();

		List<HtmlBlock> circos = template.getAllBlocks("path");
		
		long count = circos.stream()
			.filter(HtmlBlock::hasBindedClass)
			.count();
		
		Assert.assertThat((int)count, Matchers.is(NB_OF_CIRCO));
	}
	
	@Test
	public void count_577_circo_when_build_from_html() throws IOException {
		HtmlFragment template = new HtmlFragment(getLinesFromFile(BACK_SRC_MAIN_RESOURCES_CARTE_HTML), "svg");
		
		List<HtmlBlock> circos = template.getAllBlocks("path");
		
		Assert.assertThat(circos.size(), Matchers.is(NB_OF_CIRCO));
	}
	
	@Test
	@Ignore // Generates map-svg.vue for front usage.
	public void merge_vue_template_with_html() throws IOException {
		VueFile vue = new VueFile(getLinesFromFile(FRONT_SRC_COMPONENTS_MAP_MAP_SVG_VUE));
		HtmlFragment html = new HtmlFragment(getLinesFromFile(BACK_SRC_MAIN_RESOURCES_CARTE_HTML), "svg");
		html = html.add("g", "     @click=\"clicked\"");
		html = html.addBind("path", "id", "       class=\"circo {{ cssCirco('$VALUE') }}\"");
		html = html.removeLines("       class=\"circo\"");
		vue = vue.setTemplate(html);

		HtmlFragment template = vue.getTemplate();

		Assert.assertThat(template.firstLine(), Matchers.is("<div>"));
		Assert.assertThat(template.lastLine(), Matchers.is("</div>"));

		Assert.assertTrue(template.getFirstMark("g").stream()
				.map(String::trim)
				.filter(s -> "@click=\"clicked\"".equals(s))
				.findFirst()
				.map(s -> true)
				.orElse(false));

		List<HtmlBlock> circos = template.getAllBlocks("path");
		long binded = circos.stream()
				.filter(HtmlBlock::hasBindedClass)
				.count();

		File target = new File(BACK_SRC_TEST_RESOURCES_MAP_SVG_VUE);
		Files.write(target.toPath(), vue.toString().getBytes());
		
		Assert.assertThat((int)binded, Matchers.is(NB_OF_CIRCO));
		Assert.assertThat(circos.size(), Matchers.is(NB_OF_CIRCO));
	}
	
	@Test
	public void list_all_circo_id_and_names() throws IOException {
		HtmlFragment html = new HtmlFragment(getLinesFromFile(BACK_SRC_MAIN_RESOURCES_CARTE_HTML), "svg");
		List<HtmlBlock> circos = html.getAllBlocks("path");
		
		Assert.assertThat(circos.size(), Matchers.is(NB_OF_CIRCO));
		/*
		circos.stream()
			.forEach(circo -> System.out.println(circo.getId() + ", " + circo.getValue("title") + ", " + circo.getValue("desc")));
		*/
	}
	
	private List<String> getLinesFromFile(String file) throws IOException {
		return Files.lines(new File(file).toPath()).collect(Collectors.toList());
	}
}
