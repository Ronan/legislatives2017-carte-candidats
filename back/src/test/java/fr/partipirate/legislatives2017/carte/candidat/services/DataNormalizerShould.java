package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;

import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;

@RunWith(JUnitQuickcheck.class)
public class DataNormalizerShould {

    private static DataNormalizer normalizer;
    
    @BeforeClass
    public static void setup() throws IOException {
        normalizer = new DataNormalizer(new CirconscriptionsRepository());
    }
    
    @Property
    public void normalize_circos_hashtags(
            @InRange(min = "0", max = "8") int x,
            @InRange(min = "0", max = "9") int y,
            @InRange(min = "0", max = "9") int z,
            @InRange(min = "0", max = "9") int t
            ) {
        String expected = "0"+ x + y + "-" + z + t;
        Assert.assertEquals(expected, normalizer.normalize("#circo"+x+y+z+t).get(0));
        Assert.assertEquals(expected, normalizer.normalize("#Circo"+x+y+z+t).get(0));
        Assert.assertEquals(expected, normalizer.normalize(" circo"+x+y+z+t).get(0));
        Assert.assertEquals(expected, normalizer.normalize(" Circo"+x+y+z+t).get(0));
    }

    @Test
    public void normalize_etranger() {
        Assert.assertEquals(
                Arrays.asList("999-01", "999-10", "999-11"), 
                normalizer.normalize("#CircoFrEtr01, #CircoFrEtr10, #CircoFrEtr11")
                );
    }

    @Test
    public void normalize_etranger_no_zero() {
        Assert.assertEquals(
                Arrays.asList("999-03"), 
                normalizer.normalize("#CircoFrEtr3")
                );
    }

    @Test
    public void normalize_etranger_any() {
        Assert.assertEquals(
                Arrays.asList("999-01", "999-02", "999-03", "999-04", "999-05",
                              "999-06", "999-07", "999-08", "999-09", "999-10",
                              "999-11"), 
                normalizer.normalize("#CircoFrEtr")
                );
    }

    @Test
    public void normalize_etranger2() {
        Assert.assertEquals(
                Arrays.asList("999-05"), 
                normalizer.normalize("#circo9905")
                );

    }
    @Test
    public void normalize_departement() {
        Assert.assertEquals(
                Arrays.asList("072-01", "072-02", "072-03", "072-04", "072-05"), 
                normalizer.normalize("#CIRCO72")
                );
    }
    
    @Test
    public void normalize_departement_no_hash() {
        Assert.assertEquals(
                normalizer.normalize("#CIRCO75"),
                normalizer.normalize("Circo75")
                );
    }
    
    @Test
    public void normalize_typo() {
        Assert.assertEquals(
                Arrays.asList("069-01"), 
                normalizer.normalize("#69001")
                );
    }
    
    @Test
    public void normalize_nocirco_nohash() {
        Assert.assertEquals(
                Arrays.asList("068-02"), 
                normalizer.normalize("6802")
                );
    }
    
    @Test
    public void normalize_dashed() {
        Assert.assertEquals(
                Arrays.asList("059-02"), 
                normalizer.normalize("59-02")
                );
    }
    
    @Test
    public void normalize_dashed_with_3_digits() {
        Assert.assertEquals(Arrays.asList("999-01"), normalizer.normalize("999-01"));
        Assert.assertEquals(Arrays.asList("075-16"), normalizer.normalize("075-16"));
    }

    @Test
    public void normalize_dashed_with_hash() {
        Assert.assertEquals(Arrays.asList("078-07"), normalizer.normalize("#078-07"));
    }
    
    @Test
    public void normalize_corse() {
        Assert.assertEquals(
                Arrays.asList("02b-02"),
                normalizer.normalize("#Circo2B02")
                );
    }
    
    @Test
    public void normalize_typo_circo() {
        Assert.assertEquals(
                Arrays.asList("038-03"), 
                normalizer.normalize("#Circo38003")
                );
    }
    
    @Test
    public void normalize_empty() {
        Assert.assertEquals(Arrays.asList(), normalizer.normalize(null));
        Assert.assertEquals(Arrays.asList(), normalizer.normalize(""));
    }
    
    @Test
    public void normalize_guadeloupe() {
        Assert.assertEquals(
                normalizer.normalize("#circo97100"),
                normalizer.normalize("#circo97150, #circo97800")
                );
    }
    
    @Test
    public void normalize_paris() {
        Assert.assertEquals(
                normalizer.normalize("#circo75"),
                normalizer.normalize("#circoParis")
                );
    }
    
    @Test
    public void normalize_villeurbanne() {
        Assert.assertEquals(
                normalizer.normalize("Villeurbanne"),
                normalizer.normalize("#circo6906")
                );
    }
    
    @Test
    public void normalize_lyon() {
        Assert.assertEquals(
                Arrays.asList("069-01", "069-02", "069-03", "069-04"), 
                normalizer.normalize("Lyon...")
                );
    }
    
    @Test
    public void normalize_yvelines() {
        Assert.assertEquals(
                normalizer.normalize("#circo78"),
                normalizer.normalize("#circoYvelines")
                );
    }
    
    @Test
    public void split_spaces() {
        Assert.assertEquals(
                normalizer.normalize("#circo7501,#circo7502"),
                normalizer.normalize("#circo7501 #circo7502")
                );
    }
}
