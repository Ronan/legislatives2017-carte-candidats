package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.io.IOException;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.AppConfiguration;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class CirconscriptionControllerPublicShould {

	private static CirconscriptionController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circonscriptions = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circonscriptions);
		AppConfiguration config = new AppConfiguration();
		config.setDisplayMode("public");

		controller = new CirconscriptionController(
				new CandidatesRepository(normalizer),
				circonscriptions,
				normalizer,
				config 
				);
	}
	
	@Test
	public void only_return_validated_candidates() throws InvalidFormatException, IOException {
		Map<String, Circo> circos = controller.getCircos();
		
		Assert.assertEquals(577, circos.size());
		long countNotOk = circos.values().stream()
			.filter(circo -> !circo.isOk())
			.filter(circo -> 
			           circo.getCandidats().size() > 0
					|| circo.getSuppleants().size() > 0
					|| circo.getMandataires().size() > 0)
			.count();
		Assert.assertEquals(0l, countNotOk);
		
		long countOk = circos.values().stream()
				.filter(circo -> circo.isOk())
				.count();
		Assert.assertEquals(101l, countOk);
	}
}
