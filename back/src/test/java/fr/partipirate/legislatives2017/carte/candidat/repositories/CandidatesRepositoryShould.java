package fr.partipirate.legislatives2017.carte.candidat.repositories;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class CandidatesRepositoryShould {

	private static CandidatesRepository repository;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circos = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circos);
		repository = new CandidatesRepository(normalizer);
	}
	
	@Test
	public void retreive_all_candidates() {
		Assert.assertEquals(247, repository.count());
	}

	@Test
	public void retreive_a_candidate_with_id_138() {
		Candidate expected = new Candidate("138,Adeline Bartoletti,female,1,1,1,,,,#Circo6901");
		Assert.assertEquals(expected, repository.getById(138));
	}
	
	@Test
	public void retreive_a_candidate_with_id_6() {
		Candidate actual = repository.getById(6);
		Candidate expected = new Candidate("6,\"Raphael Isla\",male,1,1,,Occitanie,oui,\"Raphael Isla\",\"#Circo6501\"");
		Assert.assertEquals(expected, actual);
		Assert.assertEquals("#Circo6501", actual.getCircos());
	}
	
	@Test
	public void retreive_a_candidate_with_name_Ronan_Le_Roy() {
		Candidate candidate = repository.getByName("Ronan Le Roy");
		Assert.assertNotNull(candidate);
	}
}
