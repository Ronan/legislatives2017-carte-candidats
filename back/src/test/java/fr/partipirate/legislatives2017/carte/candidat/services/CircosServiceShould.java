package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.IOException;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircosServiceShould {

	@Test
	public void retreive_577_circo() throws IOException {
		CircosService service = new CircosService("./data");
		
		Map<String, Circo> circos = service.getCircosFromHtmlTemplate();
		
		Assert.assertThat(circos.size(), Matchers.is(577));
	}


}
