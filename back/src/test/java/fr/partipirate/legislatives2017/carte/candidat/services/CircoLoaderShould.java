package fr.partipirate.legislatives2017.carte.candidat.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import fr.partipirate.legislatives2017.carte.candidat.api.circos.Circo;

public class CircoLoaderShould {

	@Test
	public void load_data_from_source() throws InvalidFormatException, IOException {
		CircoLoader loader = Mockito.spy(new CircoLoader() {
			protected Map<String, Circo> loadFromSource(Path path) {
				return new HashMap<>();
			}
		});
		
		Path source = Paths.get(".");
		Map<String, Circo> data = loader.load(source);
		
		Assert.assertThat(data, Matchers.notNullValue());
	}
	
	@Test
	public void load_data_only_once_from_same_source() throws InvalidFormatException, IOException {
		CircoLoader loader = Mockito.spy(new CircoLoader() {
			protected Map<String, Circo> loadFromSource(Path path) {
				return new HashMap<>();
			}
		});
		
		Path source = Paths.get(".");
		Map<String, Circo> data1 = loader.load(source);
		Map<String, Circo> data2 = loader.load(source);
		
		Assert.assertThat(data1, Matchers.notNullValue());
		Assert.assertEquals(data1, data2);
		Mockito.verify(loader, Mockito.times(1)).loadFromSource(source);
	}
	
	@Test
	public void reload_data_when_source_has_changed() throws InvalidFormatException, IOException {
		CircoLoader loader = Mockito.spy(new CircoLoader() {
			protected Map<String, Circo> loadFromSource(Path path) {
				return new HashMap<>();
			}
		});
		
		File file = Mockito.mock(File.class);
		Path source = Mockito.mock(Path.class);
		Mockito.when(source.toFile()).thenReturn(file);
		
		Mockito.when(file.lastModified()).thenReturn(1l);
		loader.load(source);
		Mockito.when(file.lastModified()).thenReturn(2l);
		loader.load(source);
		
		Mockito.verify(loader, Mockito.times(2)).loadFromSource(source);
	}
	
}
