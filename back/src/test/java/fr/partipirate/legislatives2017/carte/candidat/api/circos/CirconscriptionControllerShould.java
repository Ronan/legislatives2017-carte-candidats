package fr.partipirate.legislatives2017.carte.candidat.api.circos;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.partipirate.legislatives2017.carte.candidat.AppConfiguration;
import fr.partipirate.legislatives2017.carte.candidat.beans.Candidate;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CandidatesRepository;
import fr.partipirate.legislatives2017.carte.candidat.repositories.CirconscriptionsRepository;
import fr.partipirate.legislatives2017.carte.candidat.services.DataNormalizer;

public class CirconscriptionControllerShould {

	private static CirconscriptionController controller;
	
	@BeforeClass
	public static void setup() throws IOException {
		CirconscriptionsRepository circonscriptions = new CirconscriptionsRepository();
		DataNormalizer normalizer = new DataNormalizer(circonscriptions);
		AppConfiguration config = new AppConfiguration();

		controller = new CirconscriptionController(
				new CandidatesRepository(normalizer),
				circonscriptions,
				normalizer,
				config 
				);
	}
	
	@Test
	public void list_circo_with_potential_candidates() {
		Map<String, List<Candidate>> potential = controller.allCircoWithPotentialCandidates();
		
		Assert.assertEquals(577, potential.size());
	}

	@Test
	public void list_potential_candidates_for_circos() {
		Assert.assertEquals(0, controller.listPotentialCandidates("075-01").stream().map(Candidate::getName).filter("LE ROY Ronan"::equals).count());
		Assert.assertEquals(0, controller.listPotentialCandidates("075-02").stream().map(Candidate::getName).filter("Ronan Le Roy"::equals).count());
		Assert.assertEquals(1, controller.listPotentialCandidates("029-01").stream().map(Candidate::getName).filter("Ronan Le Roy"::equals).count());
		Assert.assertEquals(1, controller.listPotentialCandidates("082-02").stream().map(Candidate::getName).filter("Cédric Levieux"::equals).count());
		Assert.assertEquals(1, controller.listPotentialCandidates("02b-02").stream().map(Candidate::getName).filter("Jean Robert-Mariani"::equals).count());
	}
	
	@Test
	public void retreive_577_circo() throws IOException, InvalidFormatException {
		Map<String, Circo> circos = controller.getCircos();
		
		Assert.assertThat(circos.size(), Matchers.is(577));
	}
	
	@Test
	public void have_a_candidate_for_02b02_without_section() throws InvalidFormatException, IOException {
		Map<String, Circo> circos = controller.getCircos();
		Circo circo = circos.get("02b-02");
		Assert.assertNotNull(circo);
		Assert.assertEquals("nosection", circo.getCss());
	}
	
	@Test 
	public void have_investiture_for_07501() throws InvalidFormatException, IOException {
		Map<String, Circo> circos = controller.getCircos();
		Circo circo = circos.get("075-01");
		Assert.assertNotNull(circo);
		Assert.assertEquals("pirate", circo.getCss());
	}
	
	@Test
	@Ignore // Generates CSV file from data.
	public void list_all_circos_to_csv() throws IOException, InvalidFormatException {
		Map<String, Circo> circos = controller.getCircos();
		List<String> lines = circos.entrySet().stream()
			.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
			.map(entry -> toCsv(entry.getKey(), entry.getValue()))
			.collect(Collectors.toList());
		
		lines.add(0, getCsvTitle());
		Files.write(Paths.get("circos.csv"), lines);
	}
	
	@Test
	@Ignore // Generates XLS file from data.
	public void list_all_circos_to_xlsx() throws FileNotFoundException, IOException, InvalidFormatException {
		Map<String, Circo> circos = controller.getCircos();
		List<String> lines = circos.entrySet().stream()
			.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
			.map(entry -> toCsv(entry.getKey(), entry.getValue()))
			.collect(Collectors.toList());
		
		lines.add(0, getCsvTitle());

		try (
				OutputStream os = new FileOutputStream(Paths.get("circos.xlsx").toFile());
				Workbook wb = new XSSFWorkbook();
			){
			Sheet sheet = wb.createSheet();
			for (int i = 0; i < lines.size(); i++) {
				Row row = sheet.createRow(i);
				String[] line = lines.get(i).split(";");
				for (int j = 0; j < line.length; j++) {
					Cell cell = row.createCell(j);
					cell.setCellValue(line[j]);
				}
			}
			wb.write(os);
		}
	}
	
	private String getCsvTitle() {
		return "Numéro;Nom;OK;Candidat;Suppleant;Etiquette;Website;Picture";
	}
	
	private String toCsv(String num, Circo circo) {
		return num+";"
				+ circo.getNom()       +";"
				+ circo.isOk()         +";"
				+ Optional.ofNullable(circo.getCandidat()).orElse("")  +";"
				+ Optional.ofNullable(circo.getSuppleant()).orElse("") +";"
				+ Optional.ofNullable(circo.getEtiquette()).orElse("") +";"
				+ Optional.ofNullable(circo.getWebsite()).orElse("")   +";"
				+ Optional.ofNullable(circo.getPicture()).orElse("")
				;
	}

}
