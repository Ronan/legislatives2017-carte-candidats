import { expect } from 'chai'

import Vue from 'vue'
Vue.config.warnExpressionErrors = false

import store from 'src/vuex/store'
import { loadLabels } from 'src/vuex/actions'

import { data } from 'src/libs/mock/fetch.mock.data'

import LoginForm from 'src/components/login/login-form'

describe('login-form', () => {
  it('should display infos, 1 buttons, 2 legends and 2 inputs', () => {
    const vm = new Vue({
      template: '<div><login-form></login-form></div>',
      components: { LoginForm },
      store: store,
      created: function () {
        this.loadLabels('/api/labels')
      },
      vuex: {
        actions: {
          loadLabels: loadLabels
        }
      }
    }).$mount()

    // console.log('comp html : ' + vm.$el.innerHTML)

    expect(vm.$el.querySelectorAll('p').length).to.equal(1)
    expect(vm.$el.querySelectorAll('p')[0].innerHTML).to.contain(data.labels.login.info)

    expect(vm.$el.querySelectorAll('button').length).to.equal(1)
    expect(vm.$el.querySelectorAll('button')[0].innerHTML).to.contain(data.labels.login.submit)

    expect(vm.$el.querySelectorAll('legend').length).to.equal(2)
    expect(vm.$el.querySelectorAll('legend')[0].innerHTML).to.contain(data.labels.login.user)
    expect(vm.$el.querySelectorAll('legend')[1].innerHTML).to.contain(data.labels.login.password)

    expect(vm.$el.querySelectorAll('input').length).to.equal(2)
  })
})
