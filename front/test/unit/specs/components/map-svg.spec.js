import { expect } from 'chai'

import Vue from 'vue'
Vue.config.warnExpressionErrors = false

import store from 'src/vuex/store'

import MapSvg from 'src/components/map/map-svg'

describe('map-svg', () => {
  it('should display the map as SVG', () => {
    const vm = new Vue({
      template: '<div><map-svg></map-svg></div>',
      components: { MapSvg },
      store: store
    }).$mount()

    // console.log('comp html : ' + vm.$el.innerHTML)

    expect(vm.$el.querySelectorAll('svg').length).to.equal(1)
  })
})
