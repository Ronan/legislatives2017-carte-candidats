// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {

  'Open home page': function (browser) {
    browser.useCss()
    browser.url('http://localhost:8080')
      .waitForElementVisible('#app', 1000)
  },

  'Check home page content': function (browser) {
    browser.useCss()
    browser
      .assert.elementPresent('svg#map')
  },
/*
  'Filling up login form': function (browser) {
    browser.useCss()
    browser.expect.element('#submit').to.not.be.enabled
    browser
      .setValue('#login', 'admin')
      .setValue('#password', 'admin')
    browser.expect.element('#submit').to.be.enabled
  },
  */
  'Closing browser': function (browser) {
    browser.end()
  }
}
