import Vue from 'vue'
import VueRouter from 'vue-router'

import PageHome from './components/home/page-home'
import PageLogin from './components/login/page-login'
import PageMap from './components/map/page-map'
import PageStats from './components/stats/page-stats'

Vue.use(VueRouter)
var router = new VueRouter()
router.map({
  '/home': {
    name: 'home',
    component: PageHome
  },
  '/login': {
    name: 'login',
    component: PageLogin
  },
  '/': {
    name: 'map',
    component: PageMap
  },
  '/stats': {
    name: 'stats',
    component: PageStats
  }
})

import App from './App'
router.start(App, '#pirate-map')
