import { fetch, post } from 'src/libs/fetch'

export const load = ({dispatch}) => {
  console.log('fetch config')
  fetch('/static/json/map-config.json',
    config => loadData({dispatch}, config),
    error => console.log(error)
  )
}

export const reload = ({dispatch}) => {
  fetch('/static/json/map-config.json',
    config => loadData({dispatch}, config),
/*
    config => {
      fetch(config.reload,
        json => loadData({dispatch}, config),
        error => console.log(error)
      )
    },
*/
    error => console.log(error)
  )
}

const loadData = ({dispatch}, config) => {
  dispatch('SET_MAP_CONFIG', config)
  loadLabels({dispatch}, config.labels)
  loadCircos({dispatch}, config.circos)
  // loadCandidatesWithoutCirco({dispatch}, config.candidatesWithoutCirco)
  // loadCircoHavingNonRegisteredCandidate({dispatch}, config.circoHavingNonRegisteredCandidate)
}

export const loadLabels = ({dispatch}, url) => {
  console.log('fetch labels : ' + url)
  fetch(url,
    json => {
      dispatch('SET_LABELS', json)
    },
    error => console.log(error)
  )
}

export const loadCircos = ({dispatch}, url) => {
  console.log('fetch circos : ' + url)
  fetch(url,
    json => {
      dispatch('SET_CIRCOS', json)
    },
    error => console.log(error)
  )
}

export const loadCandidatesWithoutCirco = ({dispatch}, url) => {
  console.log('fetch candidates without circo : ' + url)
  fetch(url,
    json => {
      dispatch('SET_CANDIDATES_WITHOUT_CIRCO', json)
    },
    error => console.log(error)
  )
}

export const loadCircoHavingNonRegisteredCandidate = ({dispatch}, url) => {
  console.log('fetch circos with non registered : ' + url)
  fetch(url,
    json => {
      dispatch('SET_CIRCO_WITH_NON_REGISTERED', json)
    },
    error => console.log(error)
  )
}

export const setSelectedCirco = ({dispatch}, circo, success) => {
  dispatch('SET_SELECTED_CIRCO', circo)
}

export const login = ({dispatch}, msg, success, error) => {
  post('/api/login',
    JSON.parse(msg),
    json => {
      if (json.auth === 'OK') {
        dispatch('SET_LOGIN', true)
        success()
      } else {
        dispatch('SET_LOGIN', false)
        error()
      }
    },
    error => console.log(error)
  )
}
