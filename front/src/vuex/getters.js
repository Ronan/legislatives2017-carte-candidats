export function isLogged (state) {
  return state.logged
}

export function getMapConfig (state) {
  return state.mapconfig
}

export function getLabels (state) {
  return state.labels
}

export function getSelected (state) {
  return state.selected
}

export function getSelectedCirco (state) {
  if (state.allcirco.hasOwnProperty(state.selected)) {
    return state.allcirco[state.selected]
  } else {
    return state.circounknown
  }
}

export function getAllCirco (state) {
  return state.allcirco
}

export function getCandidatesWithoutCirco (state) {
  return state.candidatesWithoutCirco
}

export function getCircoWithNonRegistered (state) {
  return state.circoHavingNonRegisteredCandidate
}
