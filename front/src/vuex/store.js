import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  mapconfig: {
    labels: '/static/json/labels.json',
    circos: '/static/json/circos.json'
  },
  labels: {
    login: {},
    about: {}
  },
  logged: false,
  selected: '-',
  allcirco: {},
  circounknown: {
    ok: false,
    nom: '',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  candidatesWithoutCirco: {},
  circoHavingNonRegisteredCandidate: {}
}

export const mutations = {
  SET_MAP_CONFIG (state, mapconfig) {
    state.mapconfig = mapconfig
  },
  SET_LABELS (state, labels) {
    state.labels = labels
  },
  SET_LOGIN (state, status) {
    state.logged = status
  },
  SET_CIRCOS (state, circos) {
    state.allcirco = circos
  },
  SET_SELECTED_CIRCO (state, circo) {
    state.selected = circo
  },
  SET_CANDIDATES_WITHOUT_CIRCO (state, candidates) {
    state.candidatesWithoutCirco = candidates
  },
  SET_CIRCO_WITH_NON_REGISTERED (state, circoHavingNonRegisteredCandidate) {
    state.circoHavingNonRegisteredCandidate = circoHavingNonRegisteredCandidate
  }
}

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state,
  mutations
})
