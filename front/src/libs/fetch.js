import 'whatwg-fetch'
import { mock } from './mock/fetch.mock'

export const post = (url, content, success, error) => {
  myFetch(
    url,
    {
      method: 'post',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify(content)
    })
    .then(text => text.json())
    .then(json => success(json))
    .catch(ex => error(ex))
}

export const fetch = (url, success, error) => {
  myFetch(url)
    .then(text => text.json())
    .then(json => success(json))
    .catch(ex => error(ex))
}

const myFetch = (url, request) => {
  // console.log('ENV : ' + process.env.NODE_ENV)
  if (process.env.NODE_ENV !== 'development' && process.env.NODE_ENV !== 'testing') {
    if (typeof request !== 'undefined') {
      return window.fetch(url, request)
    } else {
      return window.fetch(url)
    }
  } else {
    console.log('mock fetch for ' + url + ' (' + request + ')')
    return mock.fetch(url, request)
  }
}
