const config = {
  labels: '/api/labels',
  circos: '/api/circos',
  candidatesWithoutCirco: '/api/candidates/without-circo',
  circoHavingNonRegisteredCandidate: '/api/circo/with-non-registered'
}

const labels = {
  error: 'Error',
  login: {
    info: 'Please fill up your login and password.',
    error: 'Login and/or password are incorrect.',
    user: 'Login',
    password: 'Password',
    submit: 'Submit'
  },
  about: {
    contact: 'ronan@leroy.com',
    version: '0.0.1-SNAPSHOT'
  },
  map: {
    candidat: 'Candidat',
    suppleant: 'Suppléant',
    candidats: 'Candidats',
    suppleants: 'Suppléants',
    mandataires: 'Mandataires',
    etiquette: 'Etiquette',
    website: 'Site',
    picture: 'Photo'
  },
  stats: {
    title: 'Statistiques',
    nocirco: 'Candidats sans circonscriptions',
    nonregistered: 'Candidats non enregistrés',
    reload: 'Reload'
  }
}

const login = {
  ok: { auth: 'OK' },
  ko: { auth: 'KO' }
}

const circos = {
  '029-01': {
    ok: true,
    nom: 'Première circonscription du Finistère',
    candidat: 'Ronan Le Roy',
    suppleant: 'François Vermorel',
    etiquette: 'Breizh Pirate',
    css: 'pirate',
    website: 'https://twitter.com/ronanleroy',
    picture: 'https://pbs.twimg.com/profile_images/1083709532/avatar.jpg',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '014-01': {
    ok: true,
    nom: 'Première circonscription du Calvados',
    candidat: 'Isabelle Attard',
    suppleant: '-',
    etiquette: 'Isabelle Attard',
    css: 'lbd',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '029-03': {
    ok: false,
    nom: 'Troisième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: 'nosection',
    website: '-',
    picture: '-',
    candidats: [
      { nom: 'Ronan Le Roy' },
      { nom: 'François Vermorel' }
    ],
    suppleants: [
      { nom: 'John Doe' },
      { nom: 'Adam Smith' }
    ],
    mandataires: []
  },
  '029-04': {
    ok: false,
    nom: 'Quatrième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: '',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '029-05': {
    ok: false,
    nom: 'Cinquième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: '',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '029-06': {
    ok: false,
    nom: 'Sixième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: '',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '029-07': {
    ok: false,
    nom: 'Septième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: '',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  '029-08': {
    ok: false,
    nom: 'Huitième circonscription du Finistère',
    candidat: '-',
    suppleant: '-',
    etiquette: '-',
    css: '',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  }
}

const candidatesWithoutCirco = [
  {
    id: 1,
    name: 'Léa',
    circos: ''
  },
  {
    id: 2,
    name: 'John Doe',
    circos: '12345'
  },
  {
    id: 3,
    name: 'Sylvain De Smet',
    gender: 'MALE',
    candidate: true,
    substitute: false,
    representative: false,
    section: null,
    investi: false,
    tutor: null,
    circos: null,
    normalcircos: []
  }
]

const circoHavingNonRegisteredCandidate = [
  {
    circo: '014-01',
    ok: true,
    nom: 'Première circonscription du Calvados',
    candidat: 'Buat Isabelle',
    suppleant: '-',
    etiquette: 'La Releve Citoyenne',
    css: 'lbd',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  },
  {
    circo: '029-01',
    ok: true,
    nom: 'Première circonscription du Calvados',
    candidat: 'Buat Isabelle',
    suppleant: '-',
    etiquette: 'La Releve Citoyenne',
    css: 'lbd',
    website: '-',
    picture: '-',
    candidats: [],
    suppleants: [],
    mandataires: []
  }
]

export const data = {
  config: config,
  labels: labels,
  login: login,
  circos: circos,
  candidatesWithoutCirco: candidatesWithoutCirco,
  circoHavingNonRegisteredCandidate: circoHavingNonRegisteredCandidate
}
