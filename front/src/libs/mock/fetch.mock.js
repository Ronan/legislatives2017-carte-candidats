import { data } from './fetch.mock.data'
import { requests } from './fetch.mock.requests'

const mockedData = [
  {
    url: '/static/json/map-config.json',
    data: JSON.stringify(data.config)
  },
  {
    url: '/api/labels',
    data: JSON.stringify(data.labels)
  },
  {
    url: '/api/circos',
    data: JSON.stringify(data.circos)
  },
  {
    url: '/api/candidates/without-circo',
    data: JSON.stringify(data.candidatesWithoutCirco)
  },
  {
    url: '/api/circo/with-non-registered',
    data: JSON.stringify(data.circoHavingNonRegisteredCandidate)
  },
  {
    url: '/api/login',
    data: JSON.stringify(data.login.ko),
    requests: [
      {
        request: requests.login.ok,
        data: JSON.stringify(data.login.ok)
      }
    ]
  }
]

const promise = (data) => {
  return {
    then (func) {
      return promise(func(data))
    },
    catch () {}
  }
}

export const fakedFetch = (url, request) => {
  for (let i = 0; i < mockedData.length; i++) {
    var patt = new RegExp(mockedData[i].url)
    if (patt.test(url)) {
      // console.log('mock call to ' + url + ' will return ' + mockedData[i].data + '.')
      let result = promise({ text: () => mockedData[i].data, json: () => JSON.parse(mockedData[i].data) })
      if (request !== undefined) {
        for (let j = 0; j < mockedData[i].requests.length; j++) {
          if (request.body === JSON.stringify(mockedData[i].requests[j].request)) {
            // console.log('mock call to ' + url + '. will return ' + mockedData[i].requests[j].data + '.')
            result = promise({ text: () => mockedData[i].requests[j].data, json: () => JSON.parse(mockedData[i].requests[j].data) })
          }
        }
      }
      return result
    }
  }
}

export const mock = {
  data: data,
  fetch: fakedFetch
}
