# Projet Pirate : Legislatives 2017 - Carte des candidats

## Parent build
``` bash
mvn package
```

## Back build
``` bash
cd back
mvn package
```

## Front build
``` bash
cd front

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

For pom configuration see : http://justincalleja.com/2016/04/17/serving-a-webpack-bundle-in-spring-boot/

## COLORS

#8914CC	#CC14B4	#CC1458	#CC2D14
#2D14CC	#A733EB	#BF6AF0	#CC8914
#1458CC	#9CF06A	#76EB33	#B4CC14
#14B4CC	#14CC89	#14CC2D	#58CC14

## TODO

- Ajouter en stats : liste des candidats non enregistres.
- Ajouter en stats : nombre de candidats sur les 50 d'objectifs pour le financement.
- Ajouter en stats : nombre de candidats sur les 75 d'objectifs pour le spot de campagne.
- Ajouter en stats : pénalité pour cause de non parité.
- Différencier les mouvements Caisse Claire par CSS
- Export du fichier pour le financement.
- Export du fichier pour le spot.
